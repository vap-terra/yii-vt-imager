<?php

class ResizerAction extends CAction
{

    /*
     * public $options
     *
     * Extension's configurable options
     */
    public $options = array();

    public function run()
    {
        $defaults = array();
        $settings = array_merge($defaults, Yii::app()->imager->resizeParameters, $this->options);

        // Fetch params part
        $parts = explode(Yii::app()->imager->resizeControllerActionName, $_SERVER['REQUEST_URI']);

        $q = urldecode(end($parts));
        $q = trim($q, '/');

        // Fetch dimensions
        $parts = explode('/', $q);
        if (empty($parts[0])) {
            Yii::app()->end();
        }


        $params = explode(Yii::app()->imager->paramsDelimiter, $parts[0]);
        $sizes = explode(Yii::app()->imager->sizeDelimiter, array_shift($params));
        $type = '';

        if( !empty( $params ) ) {

            $accumulator = implode(Yii::app()->imager->paramsDelimiter,$params);
            $length = 0;
            foreach( Image::$arResizeTypes as $_type ) {
                if( mb_strpos($accumulator,$_type) === 0 && $length < mb_strlen($_type) ) {
                    $length = mb_strlen($_type);
                    $type = $_type;
                }
            }

            if( !empty( $type ) ) {
                $accumulator = trim( strtr($accumulator,[$type=>'']), Yii::app()->imager->paramsDelimiter);
            }
            if( !empty( $accumulator ) && ( $params = base64_decode($accumulator) )!==false ) {
                $params = json_decode($params,true);
                if( !empty( $params ) ) {
                    $settings = array_merge($settings, $params);
                }
            }
        }

        if( empty( $type ) ){
            $type = Yii::app()->imager->defaultResizeType;
        }

        if (empty($sizes)) {
            Yii::app()->end();
        }

        $width = intval($sizes[0]);
        $height = intval(empty($sizes[1]) ? $sizes[0] : $sizes[1]);
        if ($width < 1 || $height < 1) {
            Yii::app()->end();
        }

        // Fetch and check image path
        $pcs = explode('/', $q);
        unset($pcs[0]);
        foreach ($pcs as $k => $piece) {
            if ($piece == '..') {
                unset($pcs[$k]);
            }
        }

        $img_path = implode(DIRECTORY_SEPARATOR, $pcs);
        $original_image_path = Yii::app()->imager->storageSrc . $img_path;
        $replaceNotFound = true;

        Yii::app()->imager->displayResizeImage($width, $height, $original_image_path, $replaceNotFound, $settings, $type);
        Yii::app()->end();
    }
}