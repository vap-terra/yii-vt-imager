<?php

/**
 * Created by PhpStorm.
 * User: volhv
 * Date: 11.05.16
 * Time: 12:06
 */
setlocale(LC_ALL,'ru_RU.UTF-8');
class Imager extends CApplicationComponent
{
    public $sizeDelimiter = 'X';
    public $paramsDelimiter = '_';
    public $resizeControllerActionName = '/site/resize/';
    public $lifetime = 0;//, ms; 0 - eternal
    public $rootPath;
    public $cachePath;
    public $storageSrc = '/upload/images/';
    public $emptyImagePath;
    public $defaultBgRGBAColor = [255, 255, 255, 127];
    public $defaultResizeType = Image::RESIZE_EXACTLY;
    public $resizeParameters = [];

    /*
     *
         [
            'margin_left'=>'20%',// % | px
            'margin_bottom'=>'50%',// % | px
            'limit_width'=>'50', //%
            'limit_height'=>'100',//%
            'transparency' => '50',//%
            'filters' => [],
            'type'=>'image', //image | text | both
            'file'=>'',
            'text'=>'',
            'placement'=>'tile', // emboss | tile
        ];
    */
    public $defaultWatermark = [];
    /*
     *
        [
            'top_left'=>'',
            'bottom_left'=>'',
            'top_right'=>'',
            'bottom_right'=>'',
        ];
    */
    public $defaultCorners = [];


    /*
     *
       [
            'blur' => [
                'type'=>'gaussian', // gaussian | selective
            ],
            'grayScale' => [],
            'inverse' => [],
            'colorize' => [
                'r'=>0,
                'g'=>0,
                'b'=>0,
                'a'=>0,
            ],
            'contrast' => [
                'level' => 0,
            ],
            'brightness' => [
                'level' => 0,
            ],
            'pixelate' => [
                'blockSize' => 3,
                'improved' => false,
            ],
            'rotate' => [],
       ]
     */
    public $defaultFilters = [];


    public function getResizeImage($width, $height, $path, $replaceNotFound = true, $params = [], $type = '')
    {
        $path = rawurldecode($path);
        if (!$type) {
            $type = $this->defaultResizeType;
        }
        /*print_r( $this->rootPath );
        print_r( $this->cachePath );
        die('test');*/
        if (strpos($path, $this->rootPath) === false) {
            $path = realpath($this->rootPath . $path);
        }
        $src = trim(strtr( strtr(pathinfo($path, PATHINFO_DIRNAME),[$this->rootPath=>'']),[trim($this->storageSrc,'/')=>'']),'/').'/';
        //Имя новой картинки
        //$baseName = pathinfo($path, PATHINFO_BASENAME);
        $fileName = pathinfo($path, PATHINFO_FILENAME);
        $extension = pathinfo($path, PATHINFO_EXTENSION);
        if (empty($params)) {
            $params = [];
        }
        if (empty($params['watermark']) && !empty($this->defaultWatermark)) {
            $params['watermark'] = $this->defaultWatermark;
        }
        if (empty($params['corners']) && !empty($this->defaultCorners)) {
            $params['corners'] = $this->defaultCorners;
        }
        $names = [
            $width . $this->sizeDelimiter . $height,
            $type,
            //base64_encode(json_encode($params))
        ];
        $newFileName = $fileName . $this->paramsDelimiter . implode($this->paramsDelimiter, $names) . '.' . $extension;

        /*var_dump( $this->cachePath . $src . $newFileName );
        die();*/

        $image = null;
        try {
            $image = new Image($this->cachePath . $src . $newFileName);
            $cTime = filectime($image->getPath());
            if ($this->lifetime > 0 && $cTime < time() - $this->lifetime) {
                throw new Exception('expired cache');
            }
        } catch (Exception $e) {
            try {
                $image = new Image($path);
                $image->setArDefaultRGBABgColor($this->defaultBgRGBAColor);
                $image->resize($width, $height, $params, $type);
                if( !is_dir( $this->cachePath . $src ) ){
                    mkdir($this->cachePath . $src,0775,true);
                }
                $image->save($this->cachePath . $src . $newFileName);
            } catch (Exception $e) {
                if (!empty($this->emptyImagePath) && $replaceNotFound && $this->emptyImagePath !== $path) {
                    return $this->getResizeImage($width, $height, $this->emptyImagePath, false, [], $type);
                }
            }
        }
        return $image;
    }

    public function getSrcResizeImage($width, $height, $path, $replaceNotFound = true, $params = [], $type = '')
    {
        $image = $this->getResizeImage($width, $height, $path, $replaceNotFound, $params, $type);
        if ($image) {
            $src = $image->getPath();
            $src = '/' . trim( strtr($src, [$this->rootPath => '']), '/');
            $image->__destruct();
            return $src;
        }
        return '';
    }

    public function displayResizeImage($width, $height, $path, $replaceNotFound = true, $params = [], $type = '')
    {
        $image = $this->getResizeImage($width, $height, $path, $replaceNotFound, $params, $type);
        if ($image) {
            $image->display();
            $image->__destruct();
        }
    }

    public function getSrcResize($width, $height, $path, $replaceNotFound = true, $params = [], $type = '')
    {
        $path = rawurldecode($path);

        if (!$type) {
            $type = $this->defaultResizeType;
        }
        $names = [
            $width . $this->sizeDelimiter . $height,
            $type,
            //base64_encode(json_encode($params))
        ];
        $paramStr = implode($this->paramsDelimiter, $names);
        $src = '';
        if (!$path && $replaceNotFound) {
            $path = $this->emptyImagePath;
        }

        if ($path && strpos($path, $this->rootPath) === false) {
            $path = realpath($this->rootPath . $path);
        }

        if ($path && is_file($path)) {
            $src = '/' . trim( strtr($path, [$this->rootPath => '']), '/');
            $src = $this->resizeControllerActionName . $paramStr . '/' . trim( strtr($src, [$this->storageSrc => '']), '/' );
        }
        return $src;
    }
}
