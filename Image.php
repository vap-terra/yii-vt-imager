<?php

/**
 * Created by PhpStorm.
 * Image.php
 * @package /
 * @author Polyankin Semen Vicktorovich Vol][v, http://vap-terra.ru
 * @date 07.08.15
 * @time 11:51
 * @version 0.0.1
 * Copyright © 2014 VAP-TERRA, http://vap-terra.ru
 */
class Image
{

    //вырезать часть из центра(по умолчанию)
    //размер будет точно совпадать с заданым
    const RESIZE_CUTE = 'cut';

    //пропорционально изменить размер
    //размер не будет точно совпадать с заданым
    const RESIZE_PROPORTIONAL = 'proportional';

    //вырезать максимально допустимую пропорциональную часть из центра(по умолчанию),
    //размер не будет точно совпадать с заданным
    const RESIZE_PROPORTIONAL_CUTE = 'proportional_cute';

    //изменяет размер изображния до точно заданных размеров
    const RESIZE_EXACTLY = 'proportional_exactly';

    //
    const RESIZE_AUTO = 'auto';

    public static $arResizeTypes = [
        self::RESIZE_CUTE,
        self::RESIZE_PROPORTIONAL,
        self::RESIZE_PROPORTIONAL_CUTE,
        self::RESIZE_EXACTLY,
        self::RESIZE_AUTO
    ];

    const INCREASE = 1;//увеличить
    const REDUCE = 2;//уменьшить

    const SELECTIVE_BLUR = 'selective';
    const GAUSSIAN_BLUR = 'gaussian';

    protected $_path = '';
    protected $_extension = '';
    protected $_fileName = '';
    protected $_baseName = '';
    protected $_basePath = '';
    protected $_char = [];

    protected $_image;

    protected $_arDefaultRGBABgColor = [255, 255, 255, 127];

    protected $_arDefaultRGBABgColorForAlpha = [0, 0, 0, 127];

    /**
     * Image constructor.
     * @param $path
     * @throws Exception
     */
    public function __construct($path)
    {
        $this->_basePath = pathinfo($path, PATHINFO_DIRNAME);
        $this->_baseName = pathinfo($path, PATHINFO_BASENAME);
        $this->_fileName = pathinfo($path, PATHINFO_FILENAME);
        $this->_extension = mb_strtolower(pathinfo($path, PATHINFO_EXTENSION));
        if (!is_file($path)) {
            throw new Exception('File not found');
        }

        $this->_path = $path;

        $this->_char = @GetImageSize($path);

        if (!$this->_char || ($this->_char[0] == 0 && $this->_char[1] == 0)) {
            throw new Exception('File not supported');
        }
        /*
        Индекс 0 содержит ширину/width изображения в пикселах.
        Индекс 1 содержит высоту/height.
        Индекс 2 это флаг, указывающий тип изображения:
        1 = GIF
        2 = JPG
        3 = PNG
        4 = SWF
        5 = PSD
        6 = BMP
        7 = TIFF(байтовый порядок intel)
        8 = TIFF(байтовый порядок motorola)
        9 = JPC
        10 = JP2
        11 = JPX.
        */
        switch ($this->_char[2]) {
            case 1:
                $this->_image = imagecreatefromgif($path);
                if ($this->_image && is_resource($this->_image)) {
                    //imagealphablending($this->_image, true);
                }
                break;
            case 2:
            case 9:
            case 10:
            case 11:
                $this->_image = imageCreateFromJpeg($path);
                break;
            case 3:
                $this->_image = imageCreateFromPng($path);
                if ($this->_image && is_resource($this->_image)) {
                    //imagealphablending($this->_image, true);
                }
                break;
            case 4:
                $this->_image = imagecreatefromwbmp($path);
                break;
            default:
                throw new Exception('File not supported');
        }
        if (!$this->_image) {
            throw new Exception('File not load');
        }
    }

    /**
     * @param string $path
     * @param int $quality
     * @return bool
     * @throws Exception
     */
    public function save($path = '', $quality = 100)
    {

        if (empty($this->_image)) {
            return false;
        }

        if (!$path) {
            $path = $this->_path;
        }

        //сохроняем полученное фото
        switch ($this->_char[2]) {
            case 1:
                imagesavealpha($this->_image,true);
                imageGif($this->_image, $path);
                break;
            case 2:
            case 9:
            case 10:
            case 11:
                imageJpeg($this->_image, $path, $quality);
                break;
            case 3:
                imagesavealpha($this->_image,true);
                imagePng($this->_image, $path, 0);//$quality > 9 ? $quality / 10 - 1 : $quality);
                break;
            case 4:
                imagewBmp($this->_image, $path);
                break;
            default:
                throw new Exception('File not supported');
        }

        $this->_path = $path;
        $this->_basePath = pathinfo($path, PATHINFO_DIRNAME);
        $this->_baseName = pathinfo($path, PATHINFO_BASENAME);
        $this->_fileName = pathinfo($path, PATHINFO_FILENAME);
        $this->_extension = mb_strtolower(pathinfo($path, PATHINFO_EXTENSION));

        return true;

    }

    public function display($quality = 100)
    {
        if (empty($this->_image)) {
            return false;
        }

        /*
        Индекс 0 содержит ширину/width изображения в пикселах.
        Индекс 1 содержит высоту/height.
        Индекс 2 это флаг, указывающий тип изображения:
        1 = GIF
        2 = JPG
        3 = PNG
        4 = SWF
        5 = PSD
        6 = BMP
        7 = TIFF(байтовый порядок intel)
        8 = TIFF(байтовый порядок motorola)
        9 = JPC
        10 = JP2
        11 = JPX.
        */
        switch ($this->_char[2]) {
            case 1:
                header('content-type: image/gif');
                //чересстрочный режим
                imageInterlace($this->_image, 1);
                //цвет фона прозрачным
                imageColorTransparent($this->_image, $this->generateColor($this,$this->_arDefaultRGBABgColorForAlpha));
                imagesavealpha($this->_image,true);
                imageGif($this->_image, null);
                break;
            case 2:
            case 9:
            case 10:
            case 11:
                header('content-type: image/jpg');
                imageJpeg($this->_image, null, $quality);
                break;
            case 3:
                header('content-type: image/png');
                //чересстрочный режим
                imageInterlace($this->_image, 1);
                //цвет фона прозрачным
                imageColorTransparent($this->_image, $this->generateColor($this,$this->_arDefaultRGBABgColorForAlpha));
                imagesavealpha($this->_image,true);
                imagePng($this->_image, null, 0);// $quality > 9 ? $quality / 10 - 1 : $quality);
                break;
            case 4:
                header('content-type: image/vnd.wap.wbmp');
                imagewBmp($this->_image, null);
                break;
            default:
                throw new Exception('File not supported');
        }
    }

    public function rotate($degrees = '90', $bg = 0, $ignore_transparent = 0)
    {
        $result = imagerotate($this->_image, $degrees, $bg, $ignore_transparent);
        if (!empty($result) && is_resource($result)) {
            $this->_image = $result;
            $this->_char[0] = imagesx($this->_image);
            $this->_char[1] = imagesy($this->_image);
            return true;
        }
        return false;
    }

    public function blur($type = self::GAUSSIAN_BLUR)
    {
        if (empty($this->_image)) {
            return false;
        }
        return imagefilter($this->_image, $type == self::GAUSSIAN_BLUR ? IMG_FILTER_GAUSSIAN_BLUR : IMG_FILTER_SELECTIVE_BLUR);
    }

    public function sharpen()
    {

    }

    public function pixelate($blockSize = 3, $improved = false)
    {
        if (empty($this->_image)) {
            return false;
        }
        return imagefilter($this->_image, IMG_FILTER_PIXELATE, $blockSize, $improved);
    }

    public function brightness($level)
    {
        if (empty($this->_image)) {
            return false;
        }
        return imagefilter($this->_image, IMG_FILTER_BRIGHTNESS, $level);
    }

    public function contrast($level)
    {
        if (empty($this->_image)) {
            return false;
        }
        return imagefilter($this->_image, IMG_FILTER_CONTRAST, $level);
    }

    public function colorize($r, $g, $b, $a)
    {
        if (empty($this->_image)) {
            return false;
        }
        return imagefilter($this->_image, IMG_FILTER_COLORIZE, $r, $g, $b, $a);
    }

    public function inverse()
    {
        if (empty($this->_image)) {
            return false;
        }
        return imagefilter($this->_image, IMG_FILTER_NEGATE);
    }

    public function grayScale()
    {
        if (empty($this->_image)) {
            return false;
        }
        return imagefilter($this->_image, IMG_FILTER_GRAYSCALE);
    }

    /**
     * @param array $watermark
     * @return bool
     */
    public function watermark($watermark = [])
    {
        if (empty($this->_image) || empty($watermark)) {
            return false;
        }

        $watermarkPic = null;
        $watermark_pic_char = null;
        if (
            $watermark['type'] == 'image'
            && isset($watermark['file'])
            && is_file($watermark['file'])
        ) {
            $oWatermarkPic = new Image($watermark['file']);
            $watermarkPic = $oWatermarkPic->getImage();
            $watermark_pic_char = $oWatermarkPic->getChar();

            if( isset($watermark['limit_width']) && $watermark['limit_width']
                ||
                isset($watermark['limit_height']) && $watermark['limit_height']
            ) {
                $width = (isset($watermark['limit_width']) && $watermark['limit_width'])?$this->_char[0]*$watermark['limit_width']/100:$this->_char[0];
                $height = (isset($watermark['limit_height']) && $watermark['limit_height'])?$this->_char[1]*$watermark['limit_height']/100:$this->_char[1];
                $oWatermarkPic->resizeProportional($width,$height,[]);
                $watermarkPic = $oWatermarkPic->getImage();
                $watermark_pic_char = $oWatermarkPic->getChar();
            }

            if( !empty($watermark['filters']) ) {
                // TODO filter to watermark
            }
        }

        $marginLeft = 5;
        if (isset($watermark['margin_left'])) {
            $marginLeft = intval($watermark['margin_left']);
            if( strpos($watermark['margin_left'],'%')!==false ) {
                $marginLeft = $this->_char[0]*$marginLeft/100;
            }
        }

        $marginBottom = 5;
        if (isset($watermark['margin_bottom'])) {
            $marginBottom = intval($watermark['margin_bottom']);
            if( strpos($watermark['margin_bottom'],'%')!==false ) {
                $marginBottom = $this->_char[1]*$marginBottom/100;
            }
        }
        $transparency = 100;
        if (isset($watermark['transparency'])) {
            $transparency = intval($watermark['transparency']);
        }

        if ($watermark['type'] == 'text') {

        }

        if (
            $watermarkPic
            && is_resource($watermarkPic)
        ) {

            $watermark_width = $watermark_pic_char[0];
            $watermark_height = $watermark_pic_char[1];
            $dest_x = $this->_char[0] - $watermark_width - $marginLeft;
            $dest_y = $this->_char[1] - $watermark_height - $marginBottom;

            /*if( $watermark_pic_char[2] == 3 ){
                $bg = imagecolorallocatealpha(
                    $watermarkPic,
                    $this->_arDefaultRGBABgColor[0],
                    $this->_arDefaultRGBABgColor[1],
                    $this->_arDefaultRGBABgColor[2],
                    $this->_arDefaultRGBABgColor[3]
                ); // (PHP 4 >= 4.3.2, PHP 5)
                if (function_exists('imagecolorallocatealpha')) {
                    imagealphablending($watermarkPic, false);
                }
                imagefill($watermarkPic, 0, 0, $bg);
                if (function_exists('imagesavealpha')) {
                    imagesavealpha($watermarkPic, true);  // (PHP 4 >= 4.3.2, PHP 5)
                }
            }*/

            /*if ($this->_char[2] == 3) {
                $bg = imagecolorallocatealpha(
                    $this->_image,
                    $this->_arDefaultRGBABgColor[0],
                    $this->_arDefaultRGBABgColor[1],
                    $this->_arDefaultRGBABgColor[2],
                    $this->_arDefaultRGBABgColor[3]
                ); // (PHP 4 >= 4.3.2, PHP 5)
                if (function_exists('imagecolorallocatealpha')) {
                    imagealphablending($this->_image, false);
                }
                imagefill($this->_image, 0, 0, $bg);
                if (function_exists('imagesavealpha')) {
                    imagesavealpha($this->_image, true);  // (PHP 4 >= 4.3.2, PHP 5)
                }
            }*/

            imagealphablending($this->_image, true);
            if ($watermark_pic_char[2] == 3) {
                imagecopy($this->_image, $watermarkPic, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height);
            } else {
                imagecopymerge($this->_image, $watermarkPic, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, $transparency);
            }
            return true;
        }
        return false;
    }

    /**
     * @param array $corners
     * @return bool
     */
    public function corners($corners = [])
    {
        if (empty($this->_image) || empty($corners)) {
            return false;
        }
        imagealphablending($this->_image, true);
        //загружаем четыре уголка
        if (isset($corners['top_left'])
            && $corners['top_left']
            && is_file($corners['top_left'])
        ) {
            //Верхний левый
            $leftTop = new Image($corners['top_left']);
            $isCopy = imagecopy($this->_image, $leftTop->getImage(), 0, 0, 0, 0, $leftTop->getWidth(), $leftTop->getHeight());
        }

        if (isset($corners['bottom_left'])
            && $corners['bottom_left']
            && is_file($corners['bottom_left'])
        ) {
            //левый нижний
            $leftBottom = new Image($corners['bottom_left']);
            $isCopy = imagecopy($this->_image, $leftBottom->getImage(), 0, ($this->_char[1] - $leftBottom->getHeight()), 0, 0, $leftBottom->getWidth(), $leftBottom->getHeight());
        }

        if (isset($corners['top_right'])
            && $corners['top_right']
            && is_file($corners['top_right'])
        ) {
            //верхний правый
            $rightTop = new Image($corners['top_right']);
            $isCopy = imagecopy($this->_image, $rightTop->getImage(), $this->_char[0] - $rightTop->getWidth(), 0, 0, 0, $rightTop->getWidth(), $rightTop->getHeight());
        }

        if (isset($corners['bottom_right'])
            && $corners['bottom_right']
            && is_file($corners['bottom_right'])
        ) {
            //нижний правый
            $rightBottom = new Image($corners['bottom_right']);
            $isCopy = imagecopy($this->_image, $rightBottom, $this->_char[0] - $rightBottom->getWidth(), $this->_char[1] - $rightBottom->getHeight(), 0, 0, $rightBottom->getWidth(), $rightBottom->getHeight());
        }

        return true;
    }

    /**
     * @param $width
     * @param $height
     * @param array $params
     * @param string $type
     * @return bool
     * @throws Exception
     */
    public function resize($width, $height, $params = [], $type = self::RESIZE_PROPORTIONAL)
    {
        if (empty($this->_image)) {
            return false;
        }
        $types = explode('_', $type);
        $method = 'resize';
        foreach ($types as $name) {
            $method .= mb_strtoupper(mb_substr($name, 0, 1)) . mb_substr($name, 1);
        }
        //echo '<pre>';
        if (method_exists($this, $method)) {
            return call_user_func([$this, $method], $width, $height, $params);
        }
        //echo '</pre>';
        throw new Exception('Method not supported');
    }

    public function resizeAuto($width, $height, $params = [])
    {
        if (empty($this->_image)) {
            return false;
        }
        //TODO auto resize image
    }

    /**
     * @param $width
     * @param $height
     * @param array $params
     * @return bool
     */
    public function resizeCut($width, $height, $params = [])
    {
        //TODO make method
        if (empty($this->_image)) {
            return false;
        }

        $newSize = array($this->_char[0], $this->_char[1]);
        $needResize = self::getCutProportionalImageSize($newSize[0], $newSize[1], $width, $height);
        $newSize = array($width, $height);


        if ($needResize) {
            $nPic = imagecreatetruecolor($newSize[0], $newSize[1]);


            if ($this->_char[2] == 3 || $this->_char[2] == 1) { //png || gif
                //imagealphablending($this->_image, false);
                //Отключаем режим сопряжения цветов
                imagealphablending($nPic, false);
                $arBgColor = $this->_arDefaultRGBABgColorForAlpha;
                if($this->_char[2] == 1) {
                    $transparentIndex = imagecolortransparent($this->_image);
                    if( $transparentIndex!==-1 ){
                        $arBgColor=imagecolorsforindex($this->_image, $transparentIndex);
                        $arBgColor = [$arBgColor['red'], $arBgColor['green'], $arBgColor['blue']];
                        //Добавляем цвет в палитру нового изображения, и устанавливаем его как прозрачный
                        $transparentIndexNewPic = imagecolorallocate($nPic, $arBgColor[0], $arBgColor[1], $arBgColor[2]);
                        //На всякий случай заливаем фон этим цветом
                        //imagefill($nPic, 0, 0, $transparentIndexNewPic);
                    }
                }
            } else {
                if (empty($params['arBgColor'])) {
                    $arBgColor = $this->_arDefaultRGBABgColor;
                } else {
                    $arBgColor = $params['arBgColor'];
                }
            }
            $bg = self::generateColor($this, $arBgColor);
            imagefill($nPic, 0, 0, $bg);
            if (($this->_char[2] == 3 || $this->_char[2] == 1) && function_exists('imagesavealpha')) {  // png || gif (PHP 4 >= 4.3.2, PHP 5)
                //Включаем сохранение альфа канала
                imagesavealpha($nPic, true);
            }
            $startX = (int)($this->_char[0]/2 - $newSize[0]/2);
            $startY = (int)($this->_char[1]/2 - $newSize[1]/2);

            imagecopy( $nPic, $this->_image, 0, 0, $startX, $startY , $newSize[0], $newSize[1] );
            $this->_char[0] = $newSize[0];
            $this->_char[1] = $newSize[1];
            imageDestroy($this->_image);
            $this->_image = $nPic;
        }

        if ($this->_char[2] == 3 || $this->_char[2] == 1) {
            //imagealphablending($this->_image, true);
        }

        if (!empty($params['watermark']) && count($params['watermark'])) {
            $this->watermark($params['watermark']);
        }

        if (!empty($params['corners']) && count($params['corners'])) {
            $this->corners($params['corners']);
        }

        if( !empty($params['filters']) && count($params['filters']) ) {
            // TODO filter to image
        }

        return true;
    }

    /**
     * @param $width
     * @param $height
     * @param array $params
     * @return bool
     */
    public function resizeProportional($width, $height, $params = [])
    {
        if (empty($this->_image)) {
            return false;
        }

        $newSize = array($this->_char[0], $this->_char[1]);
        $needResize = self::getProportionalImageSize($newSize[0], $newSize[1], $width, $height);

        if ($needResize) {
            $nPic = imagecreatetruecolor($newSize[0], $newSize[1]);

            if ($this->_char[2] == 3 || $this->_char[2] == 1) { //png || gif
                //imagealphablending($this->_image, false);
                imagealphablending($nPic, false);
                $arBgColor = $this->_arDefaultRGBABgColorForAlpha;
            } else {
                if (empty($params['arBgColor'])) {
                    $arBgColor = $this->_arDefaultRGBABgColor;
                } else {
                    $arBgColor = $params['arBgColor'];
                }
            }
            $bg = self::generateColor($this, $arBgColor);
            imagefill($nPic, 0, 0, $bg);
            if (($this->_char[2] == 3 || $this->_char[2] == 1) && function_exists('imagesavealpha')) {  // png || gif (PHP 4 >= 4.3.2, PHP 5)
                //imagesavealpha($this->_image, true);
                imagesavealpha($nPic, false);
            }
            $isCopyResampled = imagecopyresampled($nPic, $this->_image, 0, 0, 0, 0, $newSize[0], $newSize[1], $this->_char[0], $this->_char[1]);
            $this->_char[0] = $newSize[0];
            $this->_char[1] = $newSize[1];
            imageDestroy($this->_image);
            $this->_image = $nPic;
        }

        if ($this->_char[2] == 3 || $this->_char[2] == 1) {
            //imagealphablending($this->_image, true);
        }

        if (!empty($params['watermark']) && count($params['watermark'])) {
            $this->watermark($params['watermark']);
        }

        if (!empty($params['corners']) && count($params['corners'])) {
            $this->corners($params['corners']);
        }

        if( !empty($params['filters']) && count($params['filters']) ) {
            // TODO filter to image
        }

        return true;
    }

    /**
     * @param $width
     * @param $height
     * @param array $params
     * @return bool
     */
    public function resizeProportionalExactly($width, $height, $params = [])
    {
        if (empty($this->_image)) {
            return false;
        }

        //пропорционально сжимаем
        if ($this->resizeProportional($width, $height, $params)) {
            //дополняем до нового размера
            $newSize = array($this->_char[0], $this->_char[1]);
            $pos = self::getProportionalExactlyImageSize($newSize[0], $newSize[1], $width, $height);
        }

        if (!empty($pos)) {
            $startCuteTop = $pos[1];
            $startCuteLeft = $pos[0];

            if (!empty($params['start_cut'])) {
                $startCute = $params['start_cut'];
                if (isset($startCute[0])) {
                    if ($startCute[0] == 'top') {
                        $startCuteTop = 0;
                    }
                }
                if (isset($startCute[1])) {
                    if ($startCute[1] == 'left') {
                        $startCuteLeft = 0;
                    }
                }
            }

            // TODO make method createNew
            $nPic = imagecreatetruecolor($newSize[0], $newSize[1]);
            if ($this->_char[2] == 3 || $this->_char[2] == 1) {
                //imagealphablending($this->_image, false);
                imagealphablending($nPic, false);
                $arBgColor = $this->_arDefaultRGBABgColorForAlpha;
            } else {
                if (empty($params['arBgColor'])) {
                    $arBgColor = $this->_arDefaultRGBABgColor;
                } else {
                    $arBgColor = $params['arBgColor'];
                }
            }
            $bg = self::generateColor($this, $arBgColor);
            imagefill($nPic, 0, 0, $bg);
            if (($this->_char[2] == 3 || $this->_char[2] == 1) && function_exists('imagesavealpha')) {  // (PHP 4 >= 4.3.2, PHP 5)
                //imagesavealpha($this->_image, true);
                imagesavealpha($nPic, false);
            }
            // end method createNew

            if ($pos[2] == self::INCREASE) { // увеличиваем изображение
                $result = imagecopy($nPic, $this->_image, $startCuteLeft, $startCuteTop, 0, 0, $this->_char[0], $this->_char[1]);
            } else { // уменьшаем изображение
                $result = imagecopy($nPic, $this->_image, 0, 0, $startCuteLeft, $startCuteTop, $newSize[0], $newSize[1]);
            }
            if ($result) {
                $this->_char[0] = $newSize[0];
                $this->_char[1] = $newSize[1];
            }

            imageDestroy($this->_image);
            $this->_image = $nPic;
            return true;
        }
        return false;
    }

    /**
     * @param $width
     * @param $height
     * @param array $params
     * @return bool
     */
    public function resizeProportionalCute($width, $height, $params = [])
    {
        if (empty($this->_image)) {
            return false;
        }
        //вырезаем максимально допустимый кусок из изображения из центра
        $newSize = array($this->_char[0], $this->_char[1]);
        $pos = self::getCutProportionalImageSize($newSize[0], $newSize[1], $width, $height);
        if ($pos) {
            $startCuteTop = $pos[1];
            $startCuteLeft = $pos[0];

            if (!empty($params['start_cut'])) {
                $startCute = $params['start_cut'];
                if (isset($startCute[0])) {
                    if ($startCute[0] == 'top') {
                        $startCuteTop = 0;
                    }
                }
                if (isset($startCute[1])) {
                    if ($startCute[1] == 'left') {
                        $startCuteLeft = 0;
                    }
                }
            }


            $nPic = imagecreatetruecolor($newSize[0], $newSize[1]);
            if ($this->_char[2] == 3 || $this->_char[2] == 1) {
                //imagealphablending($this->_image, false);
                imagealphablending($nPic, false);
                $arBgColor = $this->_arDefaultRGBABgColorForAlpha;
            } else {
                if (empty($params['arBgColor'])) {
                    $arBgColor = $this->_arDefaultRGBABgColor;
                } else {
                    $arBgColor = $params['arBgColor'];
                }
            }
            $bg = self::generateColor($this, $arBgColor);
            imagefill($nPic, 0, 0, $bg);
            if (($this->_char[2] == 3 || $this->_char[2] == 1) && function_exists('imagesavealpha')) {  // (PHP 4 >= 4.3.2, PHP 5)
                //imagesavealpha($this->_image, true);
                imagesavealpha($nPic, false);
            }
            if ($pos[2] == self::INCREASE) { // увеличиваем изображение
                $result = imagecopy($nPic, $this->_image, $startCuteLeft, $startCuteTop, 0, 0, $this->_char[0], $this->_char[1]);
            } else { // уменьшаем изображение
                $result = imagecopy($nPic, $this->_image, 0, 0, $startCuteLeft, $startCuteTop, $newSize[0], $newSize[1]);
            }
            if ($result) {
                $this->_char[0] = $newSize[0];
                $this->_char[1] = $newSize[1];
            }

            imageDestroy($this->_image);
            $this->_image = $nPic;
            //$params = [];
            return $this->resizeProportional($width, $height, $params);
        }
        return false;
    }

    public function setArDefaultRGBABgColor($arRGBAColor)
    {
        $this->_arDefaultRGBABgColor = $arRGBAColor;
    }

    public function getWidth()
    {
        if (empty($this->_image)) {
            return 0;
        }
        return imagesx($this->_image);
    }

    public function getHeight()
    {
        if (empty($this->_image)) {
            return 0;
        }
        return imagesy($this->_image);
    }

    public function getImage()
    {
        return $this->_image;
    }

    public function getChar($key = '', $default = null)
    {
        if ($key !== '') {
            if (isset($this->_char[$key])) {
                return $this->_char[$key];
            }
            return $default;
        }
        return $this->_char;
    }

    public function getPath()
    {
        return $this->_path;
    }

    public function getBasePath()
    {
        return $this->_basePath;
    }

    public function getFileName()
    {
        return $this->_fileName;
    }

    public function getBaseName()
    {
        return $this->_baseName;
    }

    public function getExtension()
    {
        return $this->_extension;
    }

    /**
     * @param $image Image
     * @param $arColor []
     * @return int
     */
    static function generateColor($image, $arColor)
    {
        $type = $image->getChar(2);
        $resource = $image->getImage();
        if (isset($arColor[3]) && function_exists('imagecolorallocatealpha') && ($type == 3 || $type == 1)) { // (PHP 4 >= 4.3.2, PHP 5)
            $bg = imagecolorallocatealpha(
                $resource,
                $arColor[0],
                $arColor[1],
                $arColor[2],
                $arColor[3]
            );
        } else {
            $bg = imagecolorallocate(
                $resource,
                $arColor[0],
                $arColor[1],
                $arColor[2]
            );
        }
        return $bg;
    }

    /**
     * Yjdst
     * @param $width
     * @param $height
     * @param $to_width
     * @param $to_height
     * @return array|bool
     */
    static function getProportionalExactlyImageSize(&$width, &$height, $to_width, $to_height)
    {
        $width = (int)$width;
        $height = (int)$height;
        $to_width = (int)$to_width;
        $to_height = (int)$to_height;
        $width_coef = $width / $to_width;
        $height_coef = $height / $to_height;

        if ($width_coef == 1 && $height_coef == 1) {//размеры совпадают
            return false;
        }

        $x = $y = 0;
        $status = self::REDUCE;

        if ($width_coef < 1 || $height_coef < 1) {//размеры изображения меньше необходимых
            $status = self::INCREASE;
            if ($width_coef < 1 && $height_coef < 1) {
                $x = (int)(($to_width - $width) / 2);
                $y = (int)(($to_height - $height) / 2);
            } else if ($width_coef < 1) {
                $x = (int)(($to_width - $width) / 2);
            } else if ($height_coef < 1) {
                $y = (int)(($to_height - $height) / 2);
            }
        } else {
            //размеры изображения больше необходимых
            if ($height_coef > $width_coef) { //если пропорция высоты  больше пропорции ширины
                $y = (int)(($to_height - $height) / 2);
            } else if ($height_coef < $width_coef) { //если пропорция высоты меньше пропорции ширины
                $x = (int)(($to_width - $width) / 2);
            }
        }

        $width = $to_width;
        $height = $to_height;

        return array($x, $y, $status);
    }

    /**
     *
     * @param $width
     * @param $height
     * @param $to_width
     * @param $to_height
     * @return array|bool
     */
    static function getCutProportionalImageSize(&$width, &$height, $to_width, $to_height)
    {
        $width = (int)$width;
        $height = (int)$height;
        $to_width = (int)$to_width;
        $to_height = (int)$to_height;
        $width_coef = $width / $to_width;
        $height_coef = $height / $to_height;

        if ($width_coef <= 1 && $height_coef <= 1) {//размеры совпадают
            return false;
        }

        $x = $y = 0;
        $status = self::REDUCE;
        //размеры изображения больше необходимых
        if ($height_coef > $width_coef) { //если пропорция высоты  больше пропорции ширины
            $y = $height;
            $height = (int)($width_coef * $to_height);
            $y = (int)(($y - $height) / 2);
        } else if ($height_coef < $width_coef) { //если пропорция высоты меньше пропорции ширины
            $x = $width;
            $width = (int)($height_coef * $to_width);
            $x = (int)(($x - $width) / 2);
        } else {
            $width = $to_width;
            $height = $to_height;
        }

        return array($x, $y, $status);
    }

    /**
     * новые размеры для пропорционального изменения
     * @param $width
     * @param $height
     * @param $to_width
     * @param $to_height
     * @return bool|int
     */
    static function getProportionalImageSize(&$width, &$height, $to_width, $to_height)
    {
        $width = (int)$width;
        $height = (int)$height;
        $to_width = (int)$to_width;
        $to_height = (int)$to_height;

        $width_coef = $width / $to_width;
        $height_coef = $height / $to_height;


        if ($width_coef <= 1 && $height_coef <= 1) {//если размер фото в допустимых границах
            return false;
        }

        $status = self::REDUCE;
        if ($height_coef > $width_coef) { //если пропорция высоты  больше пропорции ширины
            $height = (int)($height / $height_coef);
            $width = (int)($width / $height_coef);
        } else if ($height_coef < $width_coef) { //если пропорция высоты меньше пропорции ширины
            $height = (int)($height / $width_coef);
            $width = (int)($width / $width_coef);
        } else {
            $height = (int)($height / $height_coef);
            $width = (int)($width / $width_coef);
        }

        return $status;
    }

    public function __destruct()
    {
        if (!empty($this->_image) && is_resource($this->_image)) {
            imagedestroy($this->_image);
        }
        $this->_image = null;
    }
}
